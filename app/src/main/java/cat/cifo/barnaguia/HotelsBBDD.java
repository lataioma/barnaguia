package cat.cifo.barnaguia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by Alumne on 09/06/2015.
 */
public class HotelsBBDD {

    private static final String NOM_BDD = "bcn-guia.db";
    private static final int VERSION = 1;
    private static final String TAULA_HOTELS = "tblHotels";
    private static final String COL_ID = "Id";
    private static final String COL_NAME = "Nombre";
    private static final String COL_DESC = "Descripcion";
    private static final String COL_VAL = "Valoracion";
    private static final String COL_IMG1 = "Imagen1";
    private static final String COL_IMG2 = "Imagen2";
    private static final String COL_LAT = "Latitud";
    private static final String COL_LONG = "Longitud";
    private static final int NUM_COL_ID = 0;
    private static final int NUM_COL_NAME = 1;
    private static final int NUM_COL_DESC = 2;
    private static final int NUM_COL_VAL = 3;
    private static final int NUM_COL_IMG1 = 4;
    private static final int NUM_COL_IMG2 = 5;
    private static final int NUM_COL_LAT = 6;
    private static final int NUM_COL_LONG = 7;

    private SQLiteDatabase bdd;
    private HotelsBaseSQLite hotels;

    public HotelsBBDD(Context context) {
        hotels = new HotelsBaseSQLite(context, NOM_BDD, null, VERSION);
    }

    public void openForWrite() {
        bdd = hotels.getWritableDatabase();
    }
    public void openForRead() {
        bdd = hotels.getReadableDatabase();
    }
    public void close() {
        bdd.close();
    }
    public SQLiteDatabase getBbdd() {return bdd;
    }
    public long insertHotel(OHoteles hotel) {
        return bdd.insert(TAULA_HOTELS, null, createContentValues(hotel));
    }

    private ContentValues createContentValues(OHoteles hotel) {
        ContentValues content = new ContentValues();
        content.put(COL_ID, hotel.getId());
        content.put(COL_NAME, hotel.getNombre());
        content.put(COL_DESC, hotel.getDescripcion());
        content.put(COL_VAL, hotel.getValoracion());

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        BitmapDrawable img = (BitmapDrawable) hotel.getImagen1();
        img.getBitmap().compress(Bitmap.CompressFormat.PNG, 0, stream);
        content.put(COL_IMG1, stream.toByteArray());

        img = (BitmapDrawable) hotel.getImagen2();
        img.getBitmap().compress(Bitmap.CompressFormat.PNG, 0, stream);
        content.put(COL_IMG2, stream.toByteArray());
        content.put(COL_LAT, hotel.getLatitud());
        content.put(COL_LONG, hotel.getLongitud());
        return content;
    }

    public int updateHotel(int id, OHoteles hotel) {
        return bdd.update(TAULA_HOTELS, createContentValues(hotel), COL_ID + " = " +
                id, null);
    }
    public int removeHotel(String name) {
        return bdd.delete(TAULA_HOTELS, COL_NAME + " = " +
                name, null);
    }
    public OHoteles getHotel(String name) {
        Cursor c = bdd.query(TAULA_HOTELS, new String[] {
                        COL_ID, COL_NAME,COL_DESC,COL_VAL, COL_IMG1,COL_IMG2,COL_LAT,COL_LONG },COL_NAME + " LIKE \"" + name + "\"", null,
                null, null, null);
        return cursorToHotel(c);
    }

    public ArrayList<OHoteles> getAllHotels() {
        Cursor c = bdd.query(TAULA_HOTELS, new String[] {
                        COL_ID, COL_NAME,COL_DESC,COL_VAL, COL_IMG1,COL_IMG2,COL_LAT,COL_LONG },null, null,
                null, null, COL_ID);

        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        int numRows = c.getCount();
        c.close();
        ArrayList<OHoteles> hotelList = new
                ArrayList<OHoteles> ();
        for (int i = 1; i <= numRows; i++) {
            c = bdd.query(TAULA_HOTELS, new String[]{
                            COL_ID, COL_NAME, COL_DESC, COL_VAL, COL_IMG1, COL_IMG2, COL_LAT, COL_LONG}, COL_ID + " = " + i, null,
                    null, null, COL_ID);
            c.moveToFirst();
            OHoteles hotel = setHotel(c);
            hotelList.add(hotel);
            c.close();

        }
     /**
        //c.moveToFirst();
        if (c.isBeforeFirst()) Log.e("getAllHotels", "Cursor es before first");//c.moveToFirst();
        while (c.moveToNext()) {
            Log.e("getAllHotels", "Posició: " + c.getPosition());

            OHoteles hotel = setHotel(c);
            //hotelList.add(getHotel(c.getString(NUM_COL_NAME)));
            hotelList.add(hotel);
        }
        c.close();**/

        return hotelList;
    }
    public OHoteles cursorToHotel(Cursor c) {
        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        OHoteles hotel = setHotel(c);

        c.close();
        return hotel;
    }

    private OHoteles setHotel(Cursor c) {
        OHoteles hotel = new OHoteles();
        //c.moveToPosition(1);
        //c.moveToNext();
        hotel.setId(c.getInt(NUM_COL_ID));
        hotel.setNombre(c.getString(NUM_COL_NAME));
        hotel.setDescripcion(c.getString(NUM_COL_DESC));
        hotel.setValoracion(c.getInt(NUM_COL_VAL));

        //convertir imatge blob byte[] en drawable
        Bitmap bitmap1 = BitmapFactory.decodeByteArray(c.getBlob(NUM_COL_IMG1), 0, c.getBlob(NUM_COL_IMG1).length);
        //convertir bitmap a drawable
        //Drawable drw1 = new
        // BitmapDrawable(getResources(), bitmap1);
        Drawable drw1 = new BitmapDrawable(bitmap1);
        hotel.setImagen1(drw1);

        //convertir imatge blob byte[] en drawable
        Bitmap bitmap2 = BitmapFactory.decodeByteArray(c.getBlob(NUM_COL_IMG2), 0, c.getBlob(NUM_COL_IMG2).length);
        //convertir bitmap a drawable
        Drawable drw2 = new BitmapDrawable(bitmap2);
        hotel.setImagen2(drw2);

        hotel.setLatitud(c.getDouble(NUM_COL_LAT));
        hotel.setLongitud(c.getDouble(NUM_COL_LONG));
        return hotel;

    }


}