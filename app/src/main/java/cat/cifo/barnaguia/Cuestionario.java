package cat.cifo.barnaguia;

import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class Cuestionario extends ActionBarActivity implements RadioGroup.OnCheckedChangeListener{

    private String question;
    private ArrayList<String> answer;
    private Integer correct;
    private RadioGroup grupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestionario);
        question=getIntent().getExtras().getString("pregunta");
        answer=getIntent().getExtras().getStringArrayList("respuestas");
        correct=getIntent().getExtras().getInt("correcta")-1; //le quito uno para coincidir con el id radiobutton 0/1/2 y la posible respuesta 1/2/3

        TextView qst=(TextView) findViewById(R.id.pregunta);
        qst.setText(question);
        RadioButton rdb1=(RadioButton) findViewById(R.id.rb1);
        rdb1.setText(answer.get(0));
        RadioButton rdb2=(RadioButton) findViewById(R.id.rb2);
        rdb2.setText(answer.get(1));
        RadioButton rdb3=(RadioButton) findViewById(R.id.rb3);
        rdb3.setText(answer.get(2));

        RadioGroup grupo=(RadioGroup) findViewById(R.id.rgbutton);

        grupo.setOnCheckedChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cuestionario, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton rb=(RadioButton) findViewById(checkedId);
        int index=group.indexOfChild(rb);

        if(index==correct){
            rb.setBackgroundColor(getResources().getColor(R.color.correcto));
            Toast.makeText(this,"Tu respuesta es correcta",Toast.LENGTH_LONG).show();
        }
        else{
            rb.setBackgroundColor(getResources().getColor(R.color.incorrecto));
            Toast.makeText(this,"Tu respuesta es incorrecta",Toast.LENGTH_LONG).show();
        }
    }
}
