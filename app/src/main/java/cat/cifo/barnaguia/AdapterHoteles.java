package cat.cifo.barnaguia;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by usuario on 27/05/2015.
 */
public class AdapterHoteles extends BaseAdapter {
    private Context ctx;
    private ArrayList<OHoteles> items;

    public AdapterHoteles(Context ctx, ArrayList<OHoteles> items) {
        this.ctx = ctx;
        this.items = items;
    }

    public AdapterHoteles() {
    }

    @Override
    public int getCount() {return items.size();}

    @Override
    public Object getItem(int position) {return items.get(position);
    }

    @Override
    public long getItemId(int position) {return 0;}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //1 - Generar la vista
        View v = convertView;
        //2 - Llamar al objeto a mostrar
        if (convertView == null){
            LayoutInflater inf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.activity_lv_item,null);
        }
        //3 - Setear los elementos
        OHoteles hoteles = items.get(position);

        //Buscamos y asignamos los datos de cada hotel a su lugar correspondiente

        TextView txtNombre = (TextView) v.findViewById(R.id.txtNombre);
        txtNombre.setText(hoteles.getNombre());

        ImageView imgHotel = (ImageView) v.findViewById(R.id.imgHotel1);
        imgHotel.setImageDrawable(hoteles.getImagen1());

        TextView txtDescripcion = (TextView) v.findViewById(R.id.txtDescripcion);
        txtDescripcion.setText(hoteles.getDescripcion());

        RatingBar rtbEstrellas = (RatingBar) v.findViewById(R.id.rtbEstrellas);
        rtbEstrellas.setRating(hoteles.getValoracion());

        //4 - Devolver la vista
        return v;
    }
}