package cat.cifo.barnaguia;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Llistat extends Fragment {
    ArrayList<OHoteles> hoteles;
    ListView lv;
    private ListenerMissatge missatge;



    public static Llistat newInstance(Bundle arguments) {
        Llistat frag = new Llistat();
        if ( arguments !=null) {
            frag.setArguments(arguments);
        }
        return frag;
    }

    public Llistat() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //initComponents();
        hoteles = new ArrayList<OHoteles>();
        View v = inflater.inflate(R.layout.fragment_llistat, container, false);
        lv = (ListView) v.findViewById(R.id.lvLlistat);

        HotelsBBDD hBBDD = new HotelsBBDD(getActivity());
/*
        Log.e("Llistat", "vaig a insertar hotels a HotelsBBDD");
        hBBDD.openForWrite();
        hBBDD.insertHotel(new OHoteles(1, "Hotel Condal", getString(R.string.desc_hot_1), 2
                , ResourcesCompat.getDrawable(getResources(), R.drawable.hotelcondal00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.hotelcondal01, null), 41.381568, 2.174247));
        //Log.e("Llistat", "insertado hotelBBDD: " + l);
        hBBDD.insertHotel(new OHoteles(2,"Hotel Medium Monegal", getString(R.string.desc_hot_2), 2
                , ResourcesCompat.getDrawable(getResources(), R.drawable.mediummonegal00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.mediummonegal01, null), 41.385342, 2.169741));
        hBBDD.insertHotel(new OHoteles(3,"Hotel Nuevo Triunfo", getString(R.string.desc_hot_3), 2
                , ResourcesCompat.getDrawable(getResources(), R.drawable.nuevotriunfo00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.nuevotriunfo01, null), 41.373163, 2.170009));
        hBBDD.insertHotel(new OHoteles(4,"Hotel Europark", getString(R.string.desc_hot_4), 3
                , ResourcesCompat.getDrawable(getResources(), R.drawable.europark00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.europark01, null), 41.396508, 2.169874));
        hBBDD.insertHotel(new OHoteles(5,"Hotel Jazz Barcelona", getString(R.string.desc_hot_5), 3
                , ResourcesCompat.getDrawable(getResources(), R.drawable.jazz00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.jazz01, null), 41.386163, 2.165865));
        hBBDD.insertHotel(new OHoteles(6,"Hotel Condado", getString(R.string.desc_hot_6), 3
                , ResourcesCompat.getDrawable(getResources(), R.drawable.condado00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.condado01, null), 41.395616, 2.152866));
        hBBDD.insertHotel(new OHoteles(7,"Hotel Sh Abbot", getString(R.string.desc_hot_7), 4
                , ResourcesCompat.getDrawable(getResources(), R.drawable.shabbot00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.shabbot01, null), 41.381501, 2.144909));
        hBBDD.insertHotel(new OHoteles(8,"Hotel H10 Catalunya Plaza", getString(R.string.desc_hot_8), 4
                , ResourcesCompat.getDrawable(getResources(), R.drawable.h10cp00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.h10cp01, null), 41.387056, 2.168564));
        hBBDD.insertHotel(new OHoteles(9,"Hotel Colón", getString(R.string.desc_hot_9), 4
                , ResourcesCompat.getDrawable(getResources(), R.drawable.colon00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.colon01, null), 41.384957, 2.17488));
        hBBDD.insertHotel(new OHoteles(10,"Hotel Arts", getString(R.string.desc_hot_10), 5
                , ResourcesCompat.getDrawable(getResources(), R.drawable.arts00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.arts01, null), 41.386944, 2.196070));
        hBBDD.insertHotel(new OHoteles(11,"Hotel Porta Fira", getString(R.string.desc_hot_11), 5
                , ResourcesCompat.getDrawable(getResources(), R.drawable.portafira00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.portafira01, null), 41.355885, 2.125328));
        hBBDD.insertHotel(new OHoteles(12,"Hotel Hesperia Tower", getString(R.string.desc_hot_12), 5
                , ResourcesCompat.getDrawable(getResources(), R.drawable.hesperia00, null)
                , ResourcesCompat.getDrawable(getResources(), R.drawable.hesperia01, null), 41.346296, 2.108301));


        Log.e("Llistat", "insertats tots hotelsBBDDs");
        hBBDD.close();
*/
        hBBDD.openForRead();

        hoteles = hBBDD.getAllHotels();
        hBBDD.close();

        lv.setAdapter(new AdapterHoteles(getActivity(), hoteles));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //http://jarroba.com/listview-o-listado-en-android/

                OHoteles miHotel = (OHoteles) parent.getItemAtPosition(position);
               // Utilidades.showToast(getActivity(),"Imagen 1 " + miHotel.getImagen1().toString());
                //Utilidades.showToast(getActivity(),"Imagen 2 " + miHotel.getImagen2().toString());

                //Parcel pHotel;
                //miHotel.writeToParcel(pHotel);
                Bundle dades = new Bundle();
                dades.putParcelable("message",miHotel);

                missatge.dades(dades);

                /**
                String txtNom = miHotel.getNombre();
                String txtDescripcion = miHotel.getDescripcion();
                Drawable imgHotel = miHotel.getImagen();
                Integer intValoracion = miHotel.getValoracion();
**/
                //TODO tengo que crear la otra actividad que reciba la información que le mando desde este

                //String texto = "Nombre: " + txtNom + ", Descripcion:" + txtDescripcion + ". Valoracion: " + intValoracion;

                //Toast.makeText(lvLlistat.this, texto, Toast.LENGTH_LONG).show();


            }
        });
        return v; //inflater.inflate(R.layout.fragment_llistat, container, false);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            missatge = (ListenerMissatge) activity;
        }
        catch (ClassCastException e) {
            Log.e("FragmentLlistat", "Error del Try catch");
        }

    }

    public interface ListenerMissatge {
        public void dades(Bundle dades);
    }
}
