package cat.cifo.barnaguia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alumne on 09/06/2015.
 */

public class HotelsBaseSQLite extends SQLiteOpenHelper {

    private static final String TAULA_HOTELS = "tblHotels";
    private static final String COL_ID = "Id";
    private static final String COL_NAME = "Nombre";
    private static final String COL_DESC = "Descripcion";
    private static final String COL_VAL = "Valoracion";
    private static final String COL_IMG1 = "Imagen1";
    private static final String COL_IMG2 = "Imagen2";
    private static final String COL_LAT = "Latitud";
    private static final String COL_LONG = "Longitud";
    private static final String CREATE_BDD = "CREATE TABLE IF NOT EXISTS " +
            TAULA_HOTELS + " (" +
            COL_ID + " INTEGER NOT NULL," +
            COL_NAME + " TEXT NOT NULL," +
            COL_DESC + " TEXT NOT NULL," +
            COL_VAL + " INTEGER NOT NULL," +
            COL_IMG1 + " blob NOT NULL," +
            COL_IMG2 + " blob NOT NULL," +
            COL_LAT + " REAL NOT NULL," +
            COL_LONG + " REAL NOT NULL)";

    public HotelsBaseSQLite(Context context, String name,
                            SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion) {
/**En este método, debe gestionar las actualizaciones de
 versión de su base de datos**/
        db.execSQL("DROP TABLE " + TAULA_HOTELS);
        onCreate(db);
    }


}