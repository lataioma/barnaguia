package cat.cifo.barnaguia;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by usuario on 27/05/2015.
 */
public class OHoteles implements Parcelable {

    private int id;
    private String nombre;
    private String descripcion;
    private Integer valoracion;
    private Drawable imagen1;
    private Drawable imagen2;
    private Double latitud;
    private Double longitud;
    private Context ctx;

    public OHoteles (){}

    public OHoteles(int id, String nombre, String descripcion, Integer valoracion, Drawable imagen1, Drawable imagen2, Double latitud, Double longitud) {

        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.valoracion = valoracion;
        this.imagen1 = imagen1;
        this.imagen2 = imagen2;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getId() {return id;}
    public String getNombre() {return nombre;}
    public String getDescripcion() {return descripcion;}
    public Integer getValoracion() {return valoracion;}
    public Drawable getImagen1() {return imagen1;}
    public Drawable getImagen2() {return imagen2;}
    public Double getLatitud() {return latitud;}
    public Double getLongitud() {return longitud;}

    public void setId(int id) { this.id = id;}
    public void setNombre(String nombre) {this.nombre = nombre;}
    public void setDescripcion(String descripcion) {this.descripcion = descripcion;}
    public void setValoracion(Integer valoracion) {this.valoracion = valoracion;}
    public void setImagen1(Drawable imagen1) {this.imagen1 = imagen1;}
    public void setImagen2(Drawable imagen2) {this.imagen2 = imagen2;}
    public void setLatitud(Double latitud) {this.latitud = latitud;}
    public void setLongitud(Double longitud) {this.longitud = longitud;}

    protected OHoteles(Parcel in) {
        id = in.readInt();
        nombre = in.readString();
        descripcion = in.readString();
        valoracion = in.readByte() == 0x00 ? null : in.readInt();
        //imagen1 = (Drawable) in.readValue(Drawable.class.getClassLoader());
        Bitmap bitmap = (Bitmap) in.readParcelable(getClass().getClassLoader());
        imagen1 = new BitmapDrawable(ctx.getResources(),bitmap);
        //imagen2 = (Drawable) in.readValue(Drawable.class.getClassLoader());
        Bitmap bitmap2 = (Bitmap) in.readParcelable(getClass().getClassLoader());
        imagen2 = new BitmapDrawable(ctx.getResources(),bitmap2);
        latitud = in.readByte() == 0x00 ? null : in.readDouble();
        longitud = in.readByte() == 0x00 ? null : in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nombre);
        dest.writeString(descripcion);
        if (valoracion == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(valoracion);
        }
        //dest.writeValue(imagen1);
        Bitmap bitmap = (Bitmap) ((BitmapDrawable) imagen1).getBitmap();
        dest.writeParcelable(bitmap, flags);
        //dest.writeValue(imagen2);
        Bitmap bitmap2 = (Bitmap) ((BitmapDrawable) imagen2).getBitmap();
        dest.writeParcelable(bitmap2, flags);
        if (latitud == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(latitud);
        }
        if (longitud == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(longitud);
        }
    }

    @SuppressWarnings("unused")
    public static final Creator<OHoteles> CREATOR = new Creator<OHoteles>() {
        @Override
        public OHoteles createFromParcel(Parcel in) {
            return new OHoteles(in);
        }

        @Override
        public OHoteles[] newArray(int size) {
            return new OHoteles[size];
        }
    };

}