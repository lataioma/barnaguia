package cat.cifo.barnaguia;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseFile;

import java.util.ArrayList;

/**
 * Created by Alumne on 26/05/2015.
 */
public class OLugares {
    private String id;
    private ParseFile small_imagen;
    private ParseFile big_imagen;
    private String nombre;
    private String shortDescription;
    private String longDescription;
    private LatLng coordenadas;
    private String question;
    private ArrayList<String> answers;
    private int answerOK;
    private String map_marker;

    public OLugares(String id, ParseFile small_imagen, ParseFile big_imagen, String nombre, String shortDescription, String longDescription, LatLng coordenadas, String question, ArrayList<String> answers, int answerOK, String map_marker) {
        this.id = id;
        this.small_imagen = small_imagen;
        this.big_imagen = big_imagen;
        this.nombre = nombre;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.coordenadas = coordenadas;
        this.question = question;
        this.answers = answers;
        this.answerOK = answerOK;
        this.map_marker = map_marker;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ParseFile getSmall_imagen() {
        return small_imagen;
    }

    public void setSmall_imagen(ParseFile small_imagen) {
        this.small_imagen = small_imagen;
    }

    public ParseFile getBig_imagen() {
        return big_imagen;
    }

    public void setBig_imagen(ParseFile big_imagen) {
        this.big_imagen = big_imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public LatLng getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(LatLng coordenadas) {
        this.coordenadas = coordenadas;
    }

    public void setQuestion(String questions) {
        this.question = questions;
    }
    public String getQuestion() {
        return question;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public int getAnswerOK() {
        return answerOK;
    }

    public void setAnswerOK(int questionOK) {
        this.answerOK = questionOK;
    }

    public String getMap_marker() {
        return (map_marker==null?"":map_marker);
    }

    public void setMap_marker(String map_marker) {
        this.map_marker = map_marker;
    }
}