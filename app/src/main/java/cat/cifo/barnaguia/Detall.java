package cat.cifo.barnaguia;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Detall extends Fragment implements View.OnClickListener {

    public static Detall newInstance(Bundle arguments) {
        Detall frag = new Detall();
        if (arguments !=null) {
            frag.setArguments(arguments);
        }
        return frag;
    }

    public Detall() {
        // Required empty public constructor
    }

    OHoteles miHotel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detall, container, false);

        miHotel = this.getArguments().getParcelable("message");

        TextView nom = (TextView) v.findViewById(R.id.txtNombreHotel);
        nom.setText(miHotel.getNombre());

        TextView descripcio = (TextView) v.findViewById(R.id.txtDescripcionHotel);
        descripcio.setText(miHotel.getDescripcion());

        RatingBar valoracio = (RatingBar) v.findViewById(R.id.rtbEstrellas);
        valoracio.setRating(miHotel.getValoracion());

        ImageView imatge = (ImageView) v.findViewById(R.id.imgHotel1);
        imatge.setImageDrawable(miHotel.getImagen1());

        ImageView imatge2 = (ImageView) v.findViewById(R.id.imgHotel2);
        imatge2.setImageDrawable(miHotel.getImagen2());

        Button btnMapa = (Button) v.findViewById(R.id.btnMapa);
        btnMapa.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnMapa:
                Intent abrirM = new Intent(getActivity(),Mapa.class);
                abrirM.putExtra("Nombre", miHotel.getNombre());
                abrirM.putExtra("Latitud",miHotel.getLatitud());
                abrirM.putExtra("Longitud",miHotel.getLongitud());
                startActivity(abrirM.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
        }

    }


    /**@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detall, container, false);

        TextView nom = (TextView) v.findViewById(R.id.textView);
        nom.setText();

        TextView descr = (TextView) v.findViewById(R.id.textView2);

        TextView serveis = (TextView) v.findViewById(R.id.textView3);

        Drawable imatge = (Drawable) v.findViewById(R.id.imageView2);

        return v;
    }
**/

}
