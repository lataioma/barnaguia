package cat.cifo.barnaguia;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDetaLugares extends Fragment {
    private ParseImageView bigImagen;
    private TextView nombre;
    private TextView longDesc;
    private Button btnPreguntas;
    private Button btnMapa;
    private String idLugar;
    OLugares currentLugar;

    public static FragmentDetaLugares newInstance(Bundle args) {
        FragmentDetaLugares fgDetalleLugar = new FragmentDetaLugares();
        if (args != null) {
            fgDetalleLugar.setArguments(args);
        }
        return fgDetalleLugar;
    }

    public FragmentDetaLugares() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View v = null;
        //here is your arguments
        Bundle args = getArguments();
        if (args != null) {
            idLugar = args.getString("idLugar");
            currentLugar = UtilsL.getLugarFromParse(UtilsL.getParseObject("Lugares", idLugar));
            if (currentLugar != null) {
                // Inflate the layout for this fragment
                v = inflater.inflate(R.layout.fragment_fragment_deta_lugares, container, false);
                if (v != null) {
                    bigImagen = (ParseImageView) v.findViewById(R.id.imageDetPlace);
                    nombre = (TextView) v.findViewById(R.id.nombre_lugar);
                    longDesc = (TextView) v.findViewById(R.id.descr_lugar);
                    ParseFile imageFile = currentLugar.getBig_imagen();
                    if (imageFile != null) {
                        bigImagen.setParseFile(imageFile);
                        bigImagen.loadInBackground();
                    }
                    nombre.setText(currentLugar.getNombre());
                    longDesc.setText(currentLugar.getLongDescription());
                    btnPreguntas = (Button) v.findViewById(R.id.preguntas);
                    btnPreguntas.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Pasamos los datos de la pregunta
                            Bundle datosPregunta = new Bundle();
                            datosPregunta.putString("titulo", currentLugar.getNombre());
                            datosPregunta.putString("pregunta", currentLugar.getQuestion());
                            datosPregunta.putStringArrayList("respuestas", currentLugar.getAnswers());
                            datosPregunta.putInt("correcta", currentLugar.getAnswerOK());
                            // Lanzamos la actividad de preguntas
                            Intent intentPreguntas = new Intent(getActivity(), Cuestionario.class);
                            intentPreguntas.putExtras(datosPregunta);
                            startActivity(intentPreguntas);
                        }
                    });
                    btnMapa = (Button) v.findViewById(R.id.mapa);
                    btnMapa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.e("FJRP", "onClick btnMapa");
                            // Pasamos los datos de la pregunta
                            Bundle args = new Bundle();
                            //args.putParcelable("lugar", currentLugar);
                            args.putString("idLugar", currentLugar.getId());
                            // Lanzamos la actividad de preguntas
                            Intent intent = new Intent(getActivity(), MapaLugares.class);
                            intent.putExtras(args);
                            startActivity(intent);
                        }
                    });
                }
            }
        }

        return v;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);


    }


}