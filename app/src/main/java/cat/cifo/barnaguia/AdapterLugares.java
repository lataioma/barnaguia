package cat.cifo.barnaguia;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;

/**
 * Created by JoseEmilio on 04/06/2015.
 */
public class AdapterLugares extends ParseQueryAdapter<ParseObject> {

    public AdapterLugares(Context context) {
        // Use the QueryFactory to construct a PQA that will only show

        super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
            public ParseQuery create() {

                ParseQuery query = new ParseQuery("Lugares");
                query.orderByAscending("nombre");
                return query;
            }
        });
    }

    // Customize the layout by overriding getItemView
    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.activity_lv_item_lugares, null);
        }


        // Add the id view
        TextView idTextView = (TextView) v.findViewById(R.id.idRowParse);
        idTextView.setText(object.getObjectId());

        // Add the image view
        ParseImageView todoImage = (ParseImageView) v.findViewById(R.id.imagePlace);
        ParseFile imageFile = object.getParseFile("small_image");
        if (imageFile != null) {
            todoImage.setParseFile(imageFile);
            todoImage.loadInBackground();
        }

        // Add the title view
        TextView titleTextView = (TextView) v.findViewById(R.id.namePlace);
        titleTextView.setText(object.getString("nombre"));

        // Add the shortdesc view
        TextView shortDescTextView = (TextView) v.findViewById(R.id.shortDPlace);
        shortDescTextView.setText(object.getString("short_desc"));

        return v;
    }
}
