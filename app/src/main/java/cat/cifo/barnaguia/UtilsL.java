package cat.cifo.barnaguia;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by JoseEmilio on 23/06/2015.
 */
public class UtilsL {

    public static final String DB_LUGARES = "lugares.db";
    public static final int DB_LUGARES_VERSION = 1;

    public static final String TABLA_LUGARES = "tabla_lugares";
    public static final String COL_ID = "_id";
    public static final String COL_SMALL_IMAGE = "small_image";
    public static final String COL_BIG_IMAGE = "big_image";
    public static final String COL_NOMBRE = "nombre";
    public static final String COL_SHORT_DESC = "short_desc";
    public static final String COL_LONG_DESC = "long_desc";
    public static final String COL_LATITUD = "latitud";
    public static final String COL_LONGITUD = "longitud";
    public static final String COL_QUESTION = "question";
    public static final String COL_ANSWERS = "answers";
    public static final String COL_ANSWER_OK = "answer_ok";
    public static final String COL_MAP_MARKER = "map_marker";

    public static final String QUERY_CREATE_BD = "CREATE TABLE " + TABLA_LUGARES + " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_SMALL_IMAGE + " BLOB NOT NULL, " +
            COL_BIG_IMAGE + " BLOB NOT NULL, " +
            COL_NOMBRE + " TEXT NOT NULL NOT NULL, " +
            COL_SHORT_DESC + " TEXT NOT NULL, " +
            COL_LONG_DESC + " TEXT NOT NULL, " +
            COL_LATITUD + " DOUBLE NOT NULL, " +
            COL_LONGITUD + " DOUBLE NOT NULL, " +
            COL_QUESTION + " TEXT NOT NULL, " +
            COL_ANSWERS + " TEXT NOT NULL, " +
            COL_ANSWER_OK + " NUMERIC NOT NULL);";


   // public static DBLugares lugaresDBM;


    public static byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public static Bitmap byteArrayToBitmap(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static Location getLocationFromDoubles(double lat, double lng) {
        Location coord = new Location("");
        coord.setLatitude(lat);
        coord.setLongitude(lng);

        return coord;
    }

    public static ArrayList<String> createArrayListFromResponses(String strRespuestas, char separador) {
        String[] arrStr = strRespuestas.split("\\" + separador, -1);
        ArrayList<String> arrLS = new ArrayList<String>(Arrays.asList(arrStr));

        return arrLS;
    }

    public static OLugares getLugarFromParse(ParseObject lugarPO) {
        OLugares lugar = null;
        if (lugarPO != null) {
            lugar = new OLugares(
                    lugarPO.getObjectId(),
                    lugarPO.getParseFile(UtilsL.COL_SMALL_IMAGE),
                    lugarPO.getParseFile(UtilsL.COL_BIG_IMAGE),
                    lugarPO.getString(UtilsL.COL_NOMBRE),
                    lugarPO.getString(UtilsL.COL_SHORT_DESC),
                    lugarPO.getString(UtilsL.COL_LONG_DESC),
                    new LatLng(lugarPO.getDouble(UtilsL.COL_LATITUD), lugarPO.getDouble(UtilsL.COL_LONGITUD)),
                    lugarPO.getString(UtilsL.COL_QUESTION),
                    UtilsL.createArrayListFromResponses(lugarPO.getString(UtilsL.COL_ANSWERS), '|'),
                    lugarPO.getInt(UtilsL.COL_ANSWER_OK),
                    lugarPO.getString(UtilsL.COL_MAP_MARKER)
            );
        }
        return lugar;
    }

    public static ParseObject getParseObject(String tableName, String idObject) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(tableName);
        ParseObject lugarPO = null;
        try {
            lugarPO = query.get(idObject);
        }
        catch (com.parse.ParseException e) {
          // Log.e("FJRP", "Error: " + e.getMessage());
        }
        return lugarPO;

    }
    public static String strSeparator = "|";
    public static ArrayList<String> convertStringToArray(String str){
        String[] arr = str.split(strSeparator);
        ArrayList<String> alResp = new ArrayList<>();
        for (int i = 0;i<arr.length; i++) {
            alResp.add(arr[i]);
        }
        return alResp;
    }
    public static Location latlongmap(double latitud, double longitud) {
        Location localizacion=new Location("");
        localizacion.setLatitude(latitud);
        localizacion.setLongitude(longitud);

        return localizacion;
    }
}
