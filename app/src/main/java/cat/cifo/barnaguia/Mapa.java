package cat.cifo.barnaguia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class Mapa extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private Marker marker;
    OHoteles miHotel;
    ListView lv;
    ArrayList<OHoteles> hoteles;
    private LocationListener listener;
    LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        //setUpMapIfNeeded();

        //ListView
        lv = (ListView) findViewById(R.id.lvLlistat);
        HotelsBBDD hBBDD = new HotelsBBDD(Mapa.this);
        hBBDD.openForRead();
        hoteles = hBBDD.getAllHotels();
        hBBDD.close();
        lv.setAdapter(new AdapterHotelesMapa(Mapa.this, hoteles));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OHoteles miHotel = (OHoteles) parent.getItemAtPosition(position);
                Bundle dades = new Bundle();
                dades.putString("Nombre", miHotel.getNombre());
                dades.putDouble("Latitud", miHotel.getLatitud());
                dades.putDouble("Longitud", miHotel.getLongitud());
                crearMarker(dades);
            }
        });

        //Mapa
        SupportMapFragment mMap = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mMap.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap=googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        Bundle datos = this.getIntent().getExtras();
        crearMarker(datos);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void crearMarker(Bundle datos){
        mMap.clear();
        String nombre = datos.getString("Nombre");
        Double latitud = datos.getDouble("Latitud");
        Double longitud = datos.getDouble("Longitud");
        LatLng hotel = new LatLng(latitud, longitud);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hotel, 15));
        mMap.addMarker(new MarkerOptions()
                .title(nombre)
                .position(hotel)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
    }
}
