package cat.cifo.barnaguia;

import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaLugares extends AppCompatActivity implements OnMapReadyCallback, FragmentLvLugares.ListenerItemLugarClick {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private OLugares lugar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String idLugar = getIntent().getStringExtra("idLugar");
        lugar = UtilsL.getLugarFromParse(UtilsL.getParseObject("Lugares", idLugar));

        setContentView(R.layout.activity_mapa_lugares);

        SupportMapFragment mMap = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapa);
        mMap.getMapAsync(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //setUpMapIfNeeded();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMyLocationEnabled(true);
        //mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());

        gotoLugar(lugar, true);

    }

    private void gotoLugar(OLugares lugar, Boolean clearMap) {

        if (clearMap) {
            mMap.clear();
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lugar.getCoordenadas(), 17));

        int idResourceMapMarker = getResources().getIdentifier(lugar.getMap_marker(), "drawable", getPackageName());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions
                .title(lugar.getNombre())
                .position(lugar.getCoordenadas()
                );

        if (idResourceMapMarker != 0) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(idResourceMapMarker));
        } else {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        }


        mMap.addMarker(markerOptions).showInfoWindow();

    }

    /*private void loadFragmentoLvLugares() {
        // Cargamos el fragmento
        FragmentLvLugares fgLvLugares = FragmentLvLugares.newInstance(null);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_LV_lugares, fgLvLugares);

        ft.commit();
    }*/

    @Override
    public void showDetalleLugar(OLugares lugar) {
        gotoLugar(lugar, true);
    }
}
