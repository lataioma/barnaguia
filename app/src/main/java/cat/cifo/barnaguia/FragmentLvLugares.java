package cat.cifo.barnaguia;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.location.GpsStatus;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;

import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLvLugares extends Fragment {
    private Activity mActivity;
    private ParseQueryAdapter<ParseObject> mainAdapter;
    private AdapterLugares customAdapterLugaresParse;
    private ListView lvLugares;
    ListenerItemLugarClick itemClickListener; // mCallback
    ProgressDialog mProgressDialog;

    protected View v;

    // Query parameter as members ...
    private String mFromColumns;

    public static FragmentLvLugares newInstance(Bundle args) {
        FragmentLvLugares fgLvLugares = new FragmentLvLugares();
        if (args != null) {
            fgLvLugares.setArguments(args);
        }
        return fgLvLugares;
    }

    public interface ListenerItemLugarClick {
        public void showDetalleLugar(OLugares lugar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        v = inflater.inflate(R.layout.fragment_fragment_lv_lugares, container, false);

        initComponents(v);

        loadListViewLugares();
        return v;
    }


    private void initComponents(View v) {
        mActivity = getActivity();
        lvLugares = (ListView) v.findViewById(R.id.lvLugares);

    }

    // RemoteDataTask AsyncTask
    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            Log.e("FJRP", "onPreExecute");
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(getActivity());
            // Set progressdialog title
            mProgressDialog.setTitle("Parse.com Custom ListView Tutorial");
            // Set progressdialog message
            mProgressDialog.setMessage("Cargando...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.e("FJRP", "doInBackground");
            // Create the array
            try {
                loadListViewLugares();
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();
            Log.e("FJRP", "onPostExecute");
        }
    }

    private void loadListViewLugares() {

        // Initialize the subclass of ParseQueryAdapter
        customAdapterLugaresParse = new AdapterLugares(mActivity);
        customAdapterLugaresParse.setTextKey("nombre");
        customAdapterLugaresParse.setImageKey("small_image");

        // Initialize ListView and set initial view to mainAdapter
        //lvLugares = (ListView) mActivity.findViewById(R.id.lv_lugares);
        lvLugares.setAdapter(customAdapterLugaresParse);
        lvLugares.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ParseObject lugarPO = customAdapterLugaresParse.getItem(position);
                OLugares lugar = UtilsL.getLugarFromParse(lugarPO);
                itemClickListener.showDetalleLugar(lugar);

            }

        });


        //mainAdapter.loadObjects();
        customAdapterLugaresParse.loadObjects();

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            itemClickListener = (ListenerItemLugarClick) activity;
        }
        catch (ClassCastException e) {
            Log.e("onAttach-fragLvLugares", "El Activity debe implementar la interfaz ListenerMensaje");
        }
    }

}
