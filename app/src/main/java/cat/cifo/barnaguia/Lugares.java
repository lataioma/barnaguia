package cat.cifo.barnaguia;


import android.database.Cursor;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class Lugares extends ActionBarActivity implements FragmentLvLugares.ListenerItemLugarClick{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lugares);

        loadFragmentoLvLugares();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lugares, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                break;
/*            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
*/        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() != 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }

    private void loadFragmentoLvLugares() {
        // Cargamos el fragmento
        FragmentLvLugares fgLvLugares = FragmentLvLugares.newInstance(null);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.lugareslv, fgLvLugares);

        ft.commit();
    }


    @Override
    public void showDetalleLugar(OLugares lugar) {
        Bundle args = new Bundle();
        args.putString("idLugar", lugar.getId());


        FragmentDetaLugares fgDetalleLugar = FragmentDetaLugares.newInstance(args);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.lugareslv, fgDetalleLugar);
        // A�adimos la transacci�n a la pila
        ft.addToBackStack(null);
        ft.commit();
    }

    public void showDetalleLugarOLD(Cursor cursorLugar) {

        //OLugares lugar = UtilsL.lugaresDBM.getLugarFromCursor(cursorLugar);

        Bundle args = new Bundle();
        ///args.putParcelable("lugar", lugar);
        // Cargamos el fragmento
        FragmentDetaLugares fgDetalleLugar = FragmentDetaLugares.newInstance(args);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
        ft.replace(R.id.lugareslv, fgDetalleLugar);
        // A�adimos la transacci�n a la pila
        ft.addToBackStack(null);
        ft.commit();
    }
}
