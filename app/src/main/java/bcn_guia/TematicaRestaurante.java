package bcn_guia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import cat.cifo.barnaguia.R;


public class TematicaRestaurante extends AppCompatActivity implements View.OnClickListener {

    ImageView imagen1, imagen2, imagen3, imagen4, imagen5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tematica_restaurantes);
        getSupportActionBar().setTitle("Seleccion Tematica");
        copiarBaseDatos();
        initComponents();
    }

    private void initComponents() {

        imagen1 = (ImageView) findViewById(R.id.imagen_1);
        imagen2 = (ImageView) findViewById(R.id.imagen_2);
        imagen3 = (ImageView) findViewById(R.id.imagen_3);
        imagen4 = (ImageView) findViewById(R.id.imagen_4);
        imagen5 = (ImageView) findViewById(R.id.imagen_5);

        imagen1.setOnClickListener(this);
        imagen2.setOnClickListener(this);
        imagen3.setOnClickListener(this);
        imagen4.setOnClickListener(this);
        imagen5.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tematica_restaurante, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent envioDatos = new Intent(TematicaRestaurante.this, Restaurantes.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        switch (v.getId()) {
            case R.id.imagen_1:
                envioDatos.putExtra("Tematica", "Arabe");
                startActivity(envioDatos);
                break;
            case R.id.imagen_2:
                envioDatos.putExtra("Tematica", "Oriental");
                startActivity(envioDatos);
                break;
            case R.id.imagen_3:
                envioDatos.putExtra("Tematica", "FastFood");
                startActivity(envioDatos);
                break;
            case R.id.imagen_4:
                envioDatos.putExtra("Tematica", "Italiana");
                startActivity(envioDatos);
                break;
            case R.id.imagen_5:
                envioDatos.putExtra("Tematica", "Mediterranea");
                startActivity(envioDatos);
                break;
        }
    }

    private void copiarBaseDatos() {
        String ruta = "/data/data/cat.cifo.barnaguia/databases/";
        String archivo = "bcn-guia.db";
        File archivoDB = new File(ruta + archivo);
        if (!archivoDB.exists()) {
            try {
                InputStream IS = getApplicationContext().getAssets().open(archivo);
                OutputStream OS = new FileOutputStream(archivoDB);
                byte[] buffer = new byte[1024];
                int length = 0;
                while ((length = IS.read(buffer)) > 0) {
                    OS.write(buffer, 0, length);
                }
                OS.flush();
                OS.close();
                IS.close();
            } catch (FileNotFoundException e) {
                Log.e("ERROR", "Archivo no encontrado, " + e.toString());
            } catch (IOException e) {
                Log.e("ERROR", "Error al copiar la Base de Datos, " + e.toString());
            }
        }
    }
}
