package bcn_guia;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Alumne on 27/05/2015.
 */
public  class ArrayRestaurantes extends ArrayList<ORestaurantes> implements Parcelable {
    public ArrayRestaurantes() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected ArrayRestaurantes(Parcel in) {
    }

    public static final Creator<ArrayRestaurantes> CREATOR = new Creator<ArrayRestaurantes>() {
        public ArrayRestaurantes createFromParcel(Parcel source) {
            return new ArrayRestaurantes(source);
        }

        public ArrayRestaurantes[] newArray(int size) {
            return new ArrayRestaurantes[size];
        }
    };
}
