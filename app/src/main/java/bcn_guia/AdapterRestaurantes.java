package bcn_guia;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cat.cifo.barnaguia.R;

/**
 * Created by Alumne on 26/05/2015.
 */
public class AdapterRestaurantes extends BaseAdapter {
    private Context ctx;
    private ArrayList<ORestaurantes> items;

    public AdapterRestaurantes(Context ctx, ArrayList<ORestaurantes> items) {
        this.ctx = ctx;
        this.items = items;

    }

    @Override
    public int getCount() {

        return items.size();
    }

    @Override
    public Object getItem(int position) {

        return items.get(position);
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Cargamos la vista

        //Devolvemos la vista
        View v=convertView;
        if(convertView==null){
            LayoutInflater inf =(LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inf.inflate(R.layout.itemlistview,null);
        }
        //Llamamos al objeto a mostrar
        ORestaurantes Restaurantes=items.get(position);
        //seteamos los elementos en la vista
        ImageView img=(ImageView) v.findViewById(R.id.imgrestaurante);
        img.setImageBitmap(Restaurantes.getImg1());
        TextView txtNombre= (TextView) v.findViewById(R.id.tituloRestaurante);
        txtNombre.setText(Restaurantes.getNombre());
        TextView txtDireccion= (TextView) v.findViewById(R.id.direccionRestaurante);
        txtDireccion.setText(Restaurantes.getDireccion());
        TextView txtPrecio= (TextView) v.findViewById(R.id.precioRestaurante);
        txtPrecio.setText(Restaurantes.getPrecio());

        return v;
    }
}
