package bcn_guia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import cat.cifo.barnaguia.R;

public class MapaRestaurante extends AppCompatActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    static private float zoomActual;
    static private LatLng coordenadasActuales;
    static private String nom;
    static private String adresse;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_restaurante);

        double latitude, longitude;
        latitude = getIntent().getExtras().getDouble("Latitud");
        longitude = getIntent().getExtras().getDouble("Longitud");
        nom=getIntent().getExtras().getString("Nombre");
        adresse=getIntent().getExtras().getString("Direccion");
        coordenadasActuales = new LatLng(latitude, longitude);
        zoomActual = 15;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.volveratras);
        getSupportActionBar().setTitle(nom);

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {

        CameraUpdate puntoIncio = CameraUpdateFactory
                .newLatLngZoom(coordenadasActuales, zoomActual);

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.moveCamera(puntoIncio);

        BitmapDescriptor icon = BitmapDescriptorFactory
                .fromResource(R.drawable.restaurant);

        Marker marcador = mMap
                .addMarker(new MarkerOptions().position(coordenadasActuales)
                        .title(nom)
                        .snippet(adresse)
                        .icon(icon));

        marcador.showInfoWindow();
    }
}
