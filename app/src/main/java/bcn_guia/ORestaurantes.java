package bcn_guia;

import android.graphics.Bitmap;

/**
 * Created by Alumne on 26/05/2015.
 */
public class ORestaurantes {

    private String nombre;
    private String direccion;
    private String descripcion;
    private String tematica;
    private String precio;
    private String menu;
    private double latitud;
    private double longitud;
    private Bitmap Img1;
    private Bitmap Img2;
    private Bitmap Img3;
    private Bitmap Img4;
    private int id;


    public ORestaurantes(){}

    public ORestaurantes(String nombre, String direccion, String descripcion, String tematica, String precio, String menu, double latitud, double longitud, Bitmap img1, Bitmap img2, Bitmap img3, Bitmap img4, int id) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.tematica = tematica;
        this.precio = precio;
        this.menu = menu;
        this.latitud = latitud;
        this.longitud = longitud;
        this.Img1 = img1;
        this.Img2 = img2;
        this.Img3 = img3;
        this.Img4 = img4;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    public Bitmap getImg1() {
        return Img1;
    }

    public void setImg1(Bitmap img1) {
        Img1 = img1;
    }

    public Bitmap getImg2() {
        return Img2;
    }

    public void setImg2(Bitmap img2) {
        Img2 = img2;
    }

    public Bitmap getImg3() {
        return Img3;
    }

    public void setImg3(Bitmap img3) {
        Img3 = img3;
    }

    public Bitmap getImg4() {
        return Img4;
    }

    public void setImg4(Bitmap img4) {
        Img4 = img4;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }
}
