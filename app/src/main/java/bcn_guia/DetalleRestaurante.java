package bcn_guia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import cat.cifo.barnaguia.R;


public class DetalleRestaurante extends AppCompatActivity {
    ORestaurantes restaurantes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_restaurante);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.volveratras);

        //Creamos la Base de datos
        RestauranteBBDD BBDD = new RestauranteBBDD(this);
        //Recogemos la id y lanzamos la consulta
        int idRestaurante=getIntent().getExtras().getInt("restaurante");
        BBDD.openForRead();
        restaurantes = BBDD.getRestaurantesId(idRestaurante);
        getSupportActionBar().setTitle(restaurantes.getNombre());

        TextView txtDesc = (TextView) findViewById(R.id.idDescripcion);
        ImageView img1=(ImageView) findViewById(R.id.imageView2);
        ImageView img2=(ImageView) findViewById(R.id.imageView3);
        ImageView img3=(ImageView) findViewById(R.id.imageView4);
        img1.setImageBitmap(restaurantes.getImg2());
        img2.setImageBitmap(restaurantes.getImg3());
        img3.setImageBitmap(restaurantes.getImg4());
        txtDesc.setText(restaurantes.getDescripcion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detalle_restaurante, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {

            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        if (id==R.id.action_settings){
            Intent envioDatos = new Intent(DetalleRestaurante.this, MapaRestaurante.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            envioDatos.putExtra("Latitud",restaurantes.getLatitud());
            envioDatos.putExtra("Longitud",restaurantes.getLongitud());
            envioDatos.putExtra("Nombre",restaurantes.getNombre());
            envioDatos.putExtra("Direccion", restaurantes.getDireccion());
            startActivity(envioDatos);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

