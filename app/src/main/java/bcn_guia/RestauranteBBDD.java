package bcn_guia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by Alumne on 09/06/2015.
 */
public class RestauranteBBDD {

    private static final int VERSION = 2;
    private static final String NOMBRE_BBDD = "bcn-guia.db";
    private static final String TABLA_RESTAURANTES = "tabla_restaurantes";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NAME = "NOMBRE";
    private static final int NUM_COL_NAME = 1;
    private static final String COL_TEMA = "TEMATICA";
    private static final int NUM_COL_TEMA = 2;
    private static final String COL_DIR = "DIRECCION";
    private static final int NUM_COL_DIR = 3;
    private static final String COL_DESCR = "DESCRIPTION";
    private static final int NUM_COL_DESCR = 4;
    private static final String COL_PRECIO = "PRECIO";
    private static final int NUM_COL_PRECIO = 5;
    private static final String COL_MENU = "MENU";
    private static final int NUM_COL_MENU = 6;
    private static final String COL_LAT = "LATITUD";
    private static final int NUM_COL_LAT = 7;
    private static final String COL_LONG = "LONGITUD";
    private static final int NUM_COL_LONG = 8;
    private static final String COL_IMG1 = "IMAGEN1";
    private static final int NUM_COL_IMG1 = 9;
    private static final String COL_IMG2 = "IMAGEN2";
    private static final int NUM_COL_IMG2 = 10;
    private static final String COL_IMG3 = "IMAGEN3";
    private static final int NUM_COL_IMG3 = 11;
    private static final String COL_IMG4 = "IMAGEN4";
    private static final int NUM_COL_IMG4 = 12;
    private SQLiteDatabase bbdd;
    private RestauranteSQLite restaurantesBbDd;

    public RestauranteBBDD(Context context) {
        restaurantesBbDd = new RestauranteSQLite(context, NOMBRE_BBDD, null, VERSION);
    }

    public void openForWrite() {

        bbdd = restaurantesBbDd.getWritableDatabase();
    }
    public void openForRead() {

        bbdd = restaurantesBbDd.getReadableDatabase();
    }
    public void close() {

        bbdd.close();
    }
    public SQLiteDatabase getBbdd() {

        return bbdd;
    }

    public byte[] TransBitMap(Bitmap bitmap1){

        ByteArrayOutputStream baStream = new ByteArrayOutputStream();
        bitmap1.compress(Bitmap.CompressFormat.PNG,100,baStream);
        byte[] byteArray = baStream.toByteArray();
        return byteArray;
    }

    public Bitmap TransByte(byte[] byteArray){

        Bitmap bitmap2= BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        return bitmap2;
    }

    public long insertRestaurante(ORestaurantes restaurante) {
        ContentValues content = new ContentValues();
        content.put(COL_NAME, restaurante.getNombre());
        content.put(COL_TEMA, restaurante.getTematica());
        content.put(COL_DIR, restaurante.getDireccion());
        content.put(COL_DESCR, restaurante.getDescripcion());
        content.put(COL_PRECIO, restaurante.getPrecio());
        content.put(COL_MENU, restaurante.getMenu());
        content.put(COL_LAT, restaurante.getLatitud());
        content.put(COL_LONG, restaurante.getLongitud());
        content.put(COL_IMG1, TransBitMap(restaurante.getImg1()));
        content.put(COL_IMG2, TransBitMap(restaurante.getImg2()));
        content.put(COL_IMG3, TransBitMap(restaurante.getImg2()));
        content.put(COL_IMG4, TransBitMap(restaurante.getImg3()));

        return bbdd.insert(TABLA_RESTAURANTES, null, content);
    }

    public int updateRestaurante(int id, ORestaurantes restaurante) {
        ContentValues content = new ContentValues();
        content.put(COL_NAME, restaurante.getNombre());
        content.put(COL_TEMA, restaurante.getTematica());
        content.put(COL_DIR, restaurante.getDireccion());
        content.put(COL_DESCR, restaurante.getDescripcion());
        content.put(COL_PRECIO, restaurante.getPrecio());
        content.put(COL_MENU, restaurante.getMenu());
        content.put(COL_LAT, restaurante.getLatitud());
        content.put(COL_LONG, restaurante.getLongitud());
        content.put(COL_IMG1, TransBitMap(restaurante.getImg1()));
        content.put(COL_IMG2, TransBitMap(restaurante.getImg2()));
        content.put(COL_IMG3, TransBitMap(restaurante.getImg2()));
        content.put(COL_IMG4, TransBitMap(restaurante.getImg3()));

        return bbdd.update(TABLA_RESTAURANTES, content, COL_ID + " = " +
                id, null);
    }

    public int removeRestaurante(String name) {
        return bbdd.delete(TABLA_RESTAURANTES, COL_NAME + " = " +
                name, null);
    }
    public ORestaurantes getRestaurante(String name) {

        Cursor c = bbdd.query(TABLA_RESTAURANTES, new String[] { COL_ID, COL_NAME, COL_TEMA, COL_DIR, COL_DESCR, COL_PRECIO, COL_MENU, COL_LAT, COL_LONG, COL_IMG1, COL_IMG2, COL_IMG3, COL_IMG4 }
                ,COL_NAME + " LIKE \"%" + name +"%\"" , null,
                null, null, COL_NAME);


       /*Cursor c = bbdd.rawQuery("Select NOMBRE, TEMATICA, DIRECCION, DESCRIPTION, PRECIO, MENU, LATITUD, LONGITUD, IMAGEN1, IMAGEN2, IMAGEN3, IMAGEN4 " +
                " from tabla_restaurantes " +
                "where NOMBRE ='Corona Torre Altamar2'; ",null);*/

        return cursorToOrestaurante(c);
    }

    public ORestaurantes cursorToOrestaurante(Cursor c) {

        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        ORestaurantes restauranteC = new ORestaurantes();
        c.moveToFirst();
        restauranteC.setId(c.getInt(NUM_COL_ID));
        restauranteC.setNombre(c.getString(NUM_COL_NAME));
        restauranteC.setTematica(c.getString(NUM_COL_TEMA));
        restauranteC.setDireccion(c.getString(NUM_COL_DIR));
        restauranteC.setDescripcion(c.getString(NUM_COL_DESCR));
        restauranteC.setPrecio(c.getString(NUM_COL_PRECIO));
        restauranteC.setMenu(c.getString(NUM_COL_MENU));
        restauranteC.setLatitud(c.getDouble(NUM_COL_LAT));
        restauranteC.setLongitud(c.getDouble(NUM_COL_LONG));
        restauranteC.setImg1(TransByte(c.getBlob(NUM_COL_IMG1)));
        restauranteC.setImg2(TransByte(c.getBlob(NUM_COL_IMG2)));
        restauranteC.setImg3(TransByte(c.getBlob(NUM_COL_IMG3)));
        restauranteC.setImg4(TransByte(c.getBlob(NUM_COL_IMG4)));
        c.close();
        return restauranteC;
    }
    public ArrayList<ORestaurantes> getRestaurantesTematica(String tematica) {
        Cursor c = bbdd.rawQuery("Select * " +
                " from tabla_restaurantes where TEMATICA = \"" +tematica+
                "\"; ",null);
        if (c.getCount() == 0) {
            c.close();
            return null;
        }
        ArrayList<ORestaurantes> listaRestaurantes = new
                ArrayList<ORestaurantes> ();
        while (c.moveToNext()) {
            ORestaurantes restaurante1 = new ORestaurantes();
            restaurante1.setId(c.getInt(NUM_COL_ID));
            restaurante1.setNombre(c.getString(NUM_COL_NAME));
            restaurante1.setTematica(c.getString(NUM_COL_TEMA));
            restaurante1.setDireccion(c.getString(NUM_COL_DIR));
            restaurante1.setDescripcion(c.getString(NUM_COL_DESCR));
            restaurante1.setPrecio(c.getString(NUM_COL_PRECIO));
            restaurante1.setMenu(c.getString(NUM_COL_MENU));
            restaurante1.setLatitud(c.getDouble(NUM_COL_LAT));
            restaurante1.setLongitud(c.getDouble(NUM_COL_LONG));
            restaurante1.setImg1(TransByte(c.getBlob(NUM_COL_IMG1)));
            restaurante1.setImg2(TransByte(c.getBlob(NUM_COL_IMG2)));
            restaurante1.setImg3(TransByte(c.getBlob(NUM_COL_IMG3)));
            restaurante1.setImg4(TransByte(c.getBlob(NUM_COL_IMG4)));
            listaRestaurantes.add(restaurante1);
        }
        c.close();
        return listaRestaurantes;
    }
    public ORestaurantes getRestaurantesId(int idRest) {
        Cursor c = bbdd.rawQuery("Select * " +
                " from tabla_restaurantes where ID = " +idRest+
                "; ",null);

        return cursorToOrestaurante(c);
    }
}
