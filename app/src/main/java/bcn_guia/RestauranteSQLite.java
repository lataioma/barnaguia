package bcn_guia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alumne on 09/06/2015.
 */
public class RestauranteSQLite extends SQLiteOpenHelper {

    private static final String TABLA_RESTAURANTES = "tabla_restaurantes";
    private static final String COL_ID = "ID";
    private static final String COL_NAME = "NOMBRE";
    private static final String COL_TEMA = "TEMATICA";
    private static final String COL_DIR = "DIRECCION";
    private static final String COL_DESCR = "DESCRIPTION";
    private static final String COL_PRECIO = "PRECIO";
    private static final String COL_MENU = "MENU";
    private static final String COL_LAT = "LATITUD";
    private static final String COL_LONG = "LONGITUD";
    private static final String COL_IMG1 = "IMAGEN1";
    private static final String COL_IMG2 = "IMAGEN2";
    private static final String COL_IMG3 = "IMAGEN3";
    private static final String COL_IMG4 = "IMAGEN4";

    private static final String CREATE_BDD = "CREATE TABLE " +
            TABLA_RESTAURANTES + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_NAME + " TEXT NOT NULL, "
            + COL_TEMA + " TEXT NOT NULL, "
            + COL_DIR + " TEXT NOT NULL, "
            + COL_DESCR + " TEXT NOT NULL, "
            + COL_PRECIO + " INTEGER, "
            + COL_MENU + " TEXT NOT NULL, "
            + COL_LAT + " REAL NOT NULL, "
            + COL_LONG + " REAL NOT NULL, "
            + COL_IMG1 + " BLOB NOT NULL, "
            + COL_IMG2 + " BLOB NOT NULL, "
            + COL_IMG3 + " BLOB NOT NULL, "
            + COL_IMG4 + " BLOB NOT NULL);";

    public RestauranteSQLite(Context context, String name,
                             SQLiteDatabase.CursorFactory factory, int version) {
        super (context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_BDD);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion) {
        //En este metodo, debe gestionar las actualizaciones de version de su base de datos
        db.execSQL("DROP TABLE " + TABLA_RESTAURANTES);
        onCreate(db);
    }

}
