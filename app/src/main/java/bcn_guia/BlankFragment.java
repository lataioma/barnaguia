package bcn_guia;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import cat.cifo.barnaguia.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {
    protected View v;
    ArrayList<ORestaurantes> restaurantes;
    protected ListView lv;


    public static BlankFragment newInstance(Bundle args) {
        BlankFragment fragment = new BlankFragment();
        if (args!=null){
            fragment.setArguments(args);

        }
        return fragment;
    }

    public BlankFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_blank, container, false);
        //Iniciamos componentes
        initComponents();
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Creamos la Base de datos
        RestauranteBBDD BBDD = new RestauranteBBDD(getActivity());
        //Recogemos la tematica y lanzamos la consulta
        String tematica=getActivity().getIntent().getExtras().getString("Tematica");
        BBDD.openForRead();
        restaurantes = BBDD.getRestaurantesTematica(tematica);
        //Creamos el listview
        lv.setAdapter(new AdapterRestaurantes(getActivity(), restaurantes));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent envioDatosRestaurante = new Intent(getActivity(),DetalleRestaurante.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //Recogemos la posicion marcada y enviamos el id a la siguiente activity
                ORestaurantes restaurante=restaurantes.get(position);
                int idrest=restaurante.getId();
                envioDatosRestaurante.putExtra("restaurante",idrest);
                startActivity(envioDatosRestaurante);
            }
        });
    }

    private void initComponents(){
        lv=(ListView) v.findViewById(R.id.lvRestaurantes);
        restaurantes=new ArrayList<ORestaurantes>();


    }
    /*public void GeneraRestaurantes(){

        String tematica=getActivity().getIntent().getExtras().getString("Tematica");
        Toast.makeText(getActivity(), "Tematica1: " + tematica, Toast.LENGTH_SHORT).show();

        switch (tematica){
            case "Mediterranea":
                restaurantes.clear();
                restaurantes=listaRestaurantes.get("Mediterranea");
                break;
            case "FastFood":
                restaurantes.clear();
                restaurantes=listaRestaurantes.get("FastFood");
                break;
            case "Arabe":
                restaurantes.clear();
                restaurantes=listaRestaurantes.get("Arabe");
                break;
            case "Oriental":
                restaurantes.clear();
                restaurantes=listaRestaurantes.get("Arabe");
                break;
            case "Italiana":
                restaurantes.clear();
                restaurantes=listaRestaurantes.get("Arabe");
                break;
        }
    }*/


}
