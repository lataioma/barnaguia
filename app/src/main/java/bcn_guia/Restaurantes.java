package bcn_guia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import cat.cifo.barnaguia.R;


public class Restaurantes extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurantes);
        getSupportActionBar().setTitle("Comida " + getIntent().getExtras().getString("Tematica"));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.volveratras);
        BlankFragment.newInstance(null);
        /*ArrayRestaurantes listaRestaurantes=new ArrayRestaurantes(this);
        listaRestaurantes.rellenaRestaurantes();
        ArrayList<ORestaurantes> prueba=new ArrayList<>();
        prueba=listaRestaurantes.todosRestaurantes.get("Mediterranea");
        initComponents();
        //GeneraRestaurantes();
        lv.setAdapter(new AdapterRestaurantes(this,prueba));*/
    }
    /*public void GeneraRestaurantes(){
        //restaurantes.add(new ORestaurantes(getResources().getDrawable(R.drawable.arabe1), "Restaurante Arabe", "C/Valencia 41"));
        restaurantes.add(new ORestaurantes(getResources().getDrawable(R.drawable.fastfood1), "Restaurante Fast", "C/Mallorca 45"));
        restaurantes.add(new ORestaurantes(getResources().getDrawable(R.drawable.mediterranea1), "Restaurante Mediterranea", "C/Aragon 31"));
        restaurantes.add(new ORestaurantes(getResources().getDrawable(R.drawable.china), "Restaurante China", "C/Sepulveda 41"));
        restaurantes.add(new ORestaurantes(getResources().getDrawable(R.drawable.italiana), "Restaurante italiana", "C/Balmes 41"));
    }
    private void initComponents(){


    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_restaurante, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
